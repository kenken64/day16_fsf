
Blob storage - Day 16

```
#!sql

CREATE TABLE `img_storage` (
  `bid` char(16) NOT NULL,
  `comments` mediumtext,
  `upload_date` datetime NOT NULL,
  `data` longblob NOT NULL,
  `img_type` varchar(32) NOT NULL DEFAULT 'image/png',
  PRIMARY KEY (`bid`);
```