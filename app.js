var express = require("express");
var fs = require("fs");
var path = require("path");
var multer = require("multer");

var multipart = multer({dest: path.join(__dirname, "/uploads/")});
var app = express();

var bodyParser = require("body-parser");
var mysql = require("mysql");

var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "root",
    password: "password@123",
    database: "employees",
    connectionLimit: 4

});

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

const UPLOAD_SQL = "insert into img_storage (bid, comments, upload_date, data, img_type) values(?,?,?,?,?)";
const DOWNLOAD_IMG_SQL = "select bid, comments, upload_date, data, img_type from img_storage";
const PNG_FILENAME_EXT = ".png";
const PUBLIC_DOWNLOAD_DIR = "public/download/";

function handleError(err, res){
    console.log(err);
    res.status(500).end();
}

app.post("/upload", multipart.single("img-file"), function(req,res){
    console.info("Server side upload.");
    var bidId = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 15); //randomly generate the bid value.
    console.info("bidId : " + bidId);
    console.info(req.file.path);
    fs.readFile(req.file.path, function(err, data){
        pool.getConnection(function(err, conn){
            if(err){
                return handleError(err, res);
            }
            conn.query(UPLOAD_SQL,[
                bidId,
                req.body.comments,
                new Date(),
                data,
                req.file.mimetype
            ],function(err, result){
                conn.release();
                if(err){
                    console.info(err);
                    return handleError(err, res);
                }
                fs.unlinkSync(req.file.path); // delete temp files after insert into db.
                res.status(202).json({bid: bidId});
            });
        });
    });

});

app.get("/download", function(req,res){
    console.info("Server side download.");
    // get connection from the mysql pool
    pool.getConnection(function(err, connection){
        // check err against the connection
        if(err){
            return handleError(err, res);
        }
        // use connection to query the img storage table , pass in SQL no query params required
        connection.query(DOWNLOAD_IMG_SQL, [], function(err,results){
            // release connection back to the pool
            connection.release();
            //check error on sql query
            if(err){
                return handleError(err, res);
            }
            console.info("results " + results);
            // check if the sql result return to your program consist record
            if(results.length){
                // create a placerholder for your images
                var listOfImages = [];
                // loop through your result to cache the images from the db to the file system.
                results.forEach( function(item, index){
                    fs.open(PUBLIC_DOWNLOAD_DIR + index + PNG_FILENAME_EXT, 'w', 0666, function(err, file){
                        if(err){
                            console.info("error opening file " + index);
                            return handleError(err, res);
                        }
                        fs.writeFile(PUBLIC_DOWNLOAD_DIR + index + PNG_FILENAME_EXT, item.data, 'binary', function(err){
                            if(err){
                                console.info("Error writing file " + index);
                                return handleError(err, res);
                            }
                            listOfImages.push("download/" + index + PNG_FILENAME_EXT);
                            if(index == (results.length -1)){
                                // push the content out to the response for rendering on the page.
                                console.info("Images are loaded");
                                res.status(200).send(listOfImages);
                            }
                        });
                    });
                })
            }else{
                // if record is not available return a HTTP status code 404
                res.status(404).end("image " + req.params.filename + " is not available.");
            }
        });
    });
});

app.use(express.static(__dirname + "/public"));

app.listen(3000, function(){
    console.info("App Server started on port 3000");
});