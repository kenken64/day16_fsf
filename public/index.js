/**
 * Created by phangty on 20/7/16.
 */

(function(){
    angular
        .module("UploadApp", ["ngFileUpload"])
        .controller("UploadCtrl", UploadCtrl);

    UploadCtrl.$inject = ["dbService"];
    
    function UploadCtrl(dbService){
        var vm = this;
        vm.imgFile = null;
        vm.comments = "";
        vm.status = {
            message: "",
            code: 0
        };
        vm.content = [];

        vm.upload = function(){
            console.info("Client Controller uploading ...");
            dbService.uploadFileToDatabase(
                {
                     "img-file": vm.imgFile,
                     "comments": vm.comments,
                }
            ).then(function(resp){
                console.info(resp);
                vm.fileurl = resp.data.bid;
                vm.status.message = "The image is saved successfully with id" + resp.data.bid;
                vm.status.code = 202;
            }).catch(function(err){
                vm.status.message = "Failed to save file";
                vm.status.code = 400;
            });
        };

        vm.download = function(){
            console.info("Controller uploading ...");
            dbService.downloadFileFromDatabase()
                .then(function(resp){
                    console.info("resp ....");
                    vm.content = resp.data;
                }).catch(function(err){
                vm.status.message = "Failed to download file";
                vm.status.code = 400;
            });
        };
    }
})();
