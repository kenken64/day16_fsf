/**
 * Created by phangty on 20/7/16.
 */
(function(){
    angular
        .module("UploadApp")
        .service("dbService", dbService);
    
    dbService.$inject = ['$http', '$window', '$q', "Upload"];

    function dbService($http, $window, $q, Upload){
        var vm = this;
        vm.uploadFileToDatabase = function(dataToUpload) {
            console.info("Client Uploading ....");
            var defer  = $q.defer();
            Upload.upload({
               url: '/upload',
                data: dataToUpload
            }).then(function(resp){
                defer.resolve(resp);
            }).catch(function(err){
                defer.reject(err.status);
            });
            return defer.promise;
        };

        vm.downloadFileFromDatabase = function(){
            var defer=  $q.defer();
            console.info("Downloading ....");
            $http.get("/download")
                .then(function(resp){
                    defer.resolve(resp);
                }).catch(function(err){
                    defer.reject(err.status);
                });
            return defer.promise;
        };
    };
    
})();